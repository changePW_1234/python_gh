#Siddarth M
import time
from classes.df_class import df_import
from classes.file_management import file_management_source, file_management_destination

read_path = r'C:\sid_exp\test_folder' #source path to read .xlsx 
move_path = r'C:\sid_exp\test_folder\move_path' #sheet moved to this folder after sql import (will be created by program if it doesn't already exist)


#main function for importing data from all sheets in the source folder
#@time_decorator
def sql_import(path_1,path_2)  :
    #get file list
    filelist_class = file_management_source(read_path)
    filelist = filelist_class.get_filenames()    
    
    for file_ in filelist:
        print(f'Reading{file_}...')
        dfClass = df_import(path_1,file_)
        dfClass.import_data()
    filelist_ = filelist_class.get_filenames() 
    movefile_class = file_management_destination(filelist_, path_1,path_2)
    movefile_class.move_file()
    
#call main
if __name__ == "__main__":
        sql_import(read_path,move_path)
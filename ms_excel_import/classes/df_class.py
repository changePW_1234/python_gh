#Siddarth M

import mysql.connector
import pandas as pd
from collections import defaultdict
import xlwings
from openpyxl import load_workbook
import datetime
import numpy as np
import os
#Vibrant 7 database
mydb1= mysql.connector.connect(
        host = '192.168.10.121', 
        user = 'TSPI3',
        passwd = '000028',
)
mycursor1 = mydb1.cursor()

#Linux database
mydb2= mysql.connector.connect(
        host = '192.168.10.153', 
        user = 'wang',
        passwd = 'wang'
)
mycursor2 = mydb2.cursor()

#sql export class to get values for sheet
class df_export:
    def __init__(self,ppid):
        self.ppid = ppid

    #adding all previous barcodes available for a given patient id
    def add_duplicates(self,jb_list,conn):
        jb_list_added = [] 
        for jb in jb_list:
            sql1 = f"select patient_id from vibrant_america_information.sample_data where  julien_barcode = '{jb}' ;" #get patient_id
            conn.execute(sql1) ; patient_id = conn.fetchall()[0][0]
                
            sql2 = f"select julien_barcode from vibrant_america_information.sample_data where patient_id = '{patient_id}' and julien_barcode NOT LIKE '{jb}%' order by sample_collection_time;" #get list of julien_barcodes
            conn.execute(sql2) ; jb_result = conn.fetchall()
            
            if len(jb_result)>0:
                i=-1 # iterate for duplicate keyword
                for result in jb_result:
                    jb_list_added.append(f'{jb}_Duplicate_{i}_{result[0]}') #jbref_i_jbdup
                    i -= 1 #sort by desc order 
        return jb_list_added

    #get test_ids
    def get_test_details(self,test_list,conn):
        test_dict = defaultdict(lambda:"Not Present") #creating an empty dict on to which test details are to be added
        test_id_arr = [] ; k= 50000 #value set to remove test if not used
        for test in test_list:
            sql_testinfo = "select a.test_id,a.test_code,a.normal_min,a.normal_max from \
            (SELECT * FROM vibrant_america_information.report_master_list_tracking \
             where test_code = '{}' order by tracking_time desc) as a group by a.test_id;".format(test)
            
            conn.execute(sql_testinfo) ; test_info = conn.fetchall()
            try:
                test_id = test_info[0][0] ; test_min = test_info[0][2] ; test_max = test_info[0][3]
                test_dict[test] = [test_id,test_min,test_max] #adding to test details dictionary
            except Exception as e: #in case test code is missing
                print(f'{test} warning, check vibrant_america_information.report_master_list_tracking')
                test_id = k ; test_min = 1 ; test_max = 1
                test_dict[test] = [test_id,test_min,test_max]
                #k+=1
            test_id_arr.append(test_id)
        
        return test_dict,test_id_arr

    
    #adding duplicate values and removing duplicate(previous) samples with no test values
    def add_dup_values(self,df,conn2):
        test_id_arr = [] ; dup_jblist = [] ; dup_dict = defaultdict(lambda:"Not Present")
        for test_id in df['test_id']:
            if test_id not in test_id_arr:
                test_id_arr.append(test_id)
        
        for jb in df['jb']:
            try:
                if jb.split('_')[1] == 'Duplicate':
                    dup_jblist.append(jb.split('_')[3])
                    dup_dict[jb.split('_')[3]] = jb
            except Exception as e:
                exception = e
        
        #print(dup_jblist)
        #sql string formatting
        test_id_string = '' ; jb_string = '' ; i=0 ; j = 0
        
        for test_id in test_id_arr:   
            i+=1
            if i<len(test_id_arr):
                test_id_string+= str(test_id)+','
            else:
                test_id_string+= str(test_id)
    
        for jb in dup_jblist: 
            j+=1
            if j<len(dup_jblist):
                
                jb_string+= str(jb)+','
            else:
                jb_string+= str(jb)
        
        sql_dupvals = "select julien_barcode,TEST_ID,result from `vibrant_america_test_result`.`sample_test_result` where test_id in \
        ({}) and julien_barcode in ({})".format(test_id_string,jb_string)
        
        conn2.execute(sql_dupvals) ; dupvals = conn2.fetchall()
        
        for val in dupvals:
            df.loc[(df.jb==dup_dict[val[0]])&(df.test_id==val[1]),'unit'] = val[2]
            #print(dup_dict)
        
        df_withdup = df.loc[df.unit!=0] # removing sections with all zeros
        
        return df_withdup,dupvals

    #getting list of collection times
    def add_collection_time(self,df,jb_list,conn):
        collection_time =[]
        for jb in jb_list:
            try:
                jb_dup = jb.split('_')[3] #jbdup
                jb_=jb_dup 
            except Exception:
                jb_ = jb
                
            sql_ct = f"select sample_collection_time from vibrant_america_information.sample_data where julien_barcode = '{jb_}'"; #get collection time
            conn.execute(sql_ct) ; ct = conn.fetchall()[0][0] ; ct_formatted = str(ct)[:10]
            collection_time.append(ct_formatted)
            df.loc[df.jb==jb,'ctime'] = ct_formatted
        return df,collection_time    

    #creating dataframe with unit values
    def create_dF(self):
        ppid = self.ppid
        conn1 = mycursor1
        conn2 = mycursor2
        #get values from database
        sql = "select * from tsp_test_unit_data.test_unit_data where pillar_plate_id = '{}' order by test_name,julien_barcode".format(ppid) 
        conn1.execute(sql)
        sql_values = conn1.fetchall() #(test_name,julien_barcode,raw_unit,unit,pillar_plate_id,row,col,error_code,time)
        
        storage = [] ; testnames = [] ; jb_list_ = [] ; jb_list = []  #jb_list created after ordering with duplicates
        for val in sql_values:
            well = val[5] + str(val[6])
            temp = [str(val[0]),str(val[1]),float(val[3]),well] #'test_name','jb','unit'        
            storage.append(temp)
            
            #get list of testnames
            if val[0] not in testnames:
                testnames.append(val[0])
            
            #get barcode list
            if val[1] not in jb_list_:
                jb_list_.append(val[1])
    
        duplicate_list = self.add_duplicates(jb_list_,conn2)
        
        test_list = [] #array with unique test names
        for test in testnames:
            if test not in test_list:
                test_list.append(test)
        
        if len(duplicate_list) > 0:
            for jb in duplicate_list:
                for test in test_list:
                    storage.append([test,jb,float(0),'I1']) #temporariy add testnames and barcodes with 0 value for duplicates
        
        df_columns = ['test_name','jb','unit','well']
        df = pd.DataFrame(data=storage, columns=df_columns)
        df.sort_values(by=['jb','test_name'], inplace=True) #sorting after adding duplicate values
        
        df = df.set_index('jb', drop=False)
        test_info_dict,test_id = self.get_test_details(testnames,conn2) #use test_info_dict to enter low and high values for each test, and use
        
        
        #adding test_ids to dataframe
        test_id_arr = []
        for test in df['test_name']:
            t_id = test_info_dict[test][0]
            test_id_arr.append(t_id)
        
    
        df['test_id'] = test_id_arr
        #print(test_id_arr)
        df = df.loc[df.test_id < 50000] #remove tests not in report_master_list_tracking
        df_withdup,dupvals = self.add_dup_values(df,conn2)
        
        #get list of barcodes --- use list after adding duplicates
        for jb in df_withdup['jb']:
            if jb not in jb_list:
                jb_list.append(jb)

        df_withdup = df
        #adding collection time
        df_withdup,collection_time_arr = self.add_collection_time(df_withdup,jb_list,conn2)
            
        
        #remove tests not in report_master_list_tracking
        del_list = []
        for test in test_info_dict:
            if test_info_dict[test][0] == 50000:
                if test not in del_list: (del_list.append(test))
        for test in del_list: del test_info_dict[test]
        
        #set empty duplicate values as nan
        for jb in df_withdup['jb']:
            try: 
                if jb.split('_')[1] == 'Duplicate':
                    df_withdup.loc[(df_withdup.jb==jb) & (df_withdup.unit==0),'unit'] = np.nan
            except Exception:
                pass
            
        return df_withdup, jb_list, collection_time_arr, test_info_dict

#sql import class to import sheet values        
class df_import:
    def __init__(self,source,file):
        self.source = source
        self.file = file
        
    #need to send path+file.xlsx into arguements
    def df_from_excel(self,path):
        app = xlwings.App(visible=False)
        book = app.books.open(path)
        book.save()
        app.kill()
        return pd.read_excel(path)
    
    def import_data(self):
        file = self.file
        path = self.source
        conn = mycursor1
        
        df = self.df_from_excel(path+'\\'+file).head(95) #get first 95 values, ignore reference values
        df = df.set_index('TestCode',drop=True)
        file_ = file.split('.')[0] + '_.xlsx'
        df.to_excel(file_)
        
        wb = load_workbook(file_,data_only=True) 
        ws = wb['Sheet1']
        time_now = str(datetime.datetime.now())
        
        ppid = ws['A2'].value #pillar_plate_id
        jb_list = [] ; testnames = []
        
        ##delete existing entries to allow for reupload if changes have been made
        sql_delete = "delete from tsp_test_unit_data.test_unit_data where pillar_plate_id = '{}'".format(ppid); 
        conn.execute(sql_delete)
        mydb1.commit()            
        
        #getting list of barcodes and testnames
        c = 0 ; r =0 ; flag1 = 0 ; flag2 = 0
        for jb_ in ws['1':'1']: #getting barcodes from row 1
            c+=1
            if c == 7: #c[7,n1]
                flag1 = 1
            
            if flag1 == 1:
                jb_list.append(jb_.value)
                
        #getting list of testnames
        for test_ in ws['A':'A']: #getting testnames from column 1
            r+=1
            if r == 4: #r[4,n2]
                flag2 = 1
            
            if r == 99:
                flag2 = 0
            
            if flag2==1:
                if test_.value != None: 
                    testnames.append(test_.value)
        
        r = 4
        for test in testnames:
            c=7;
            
            for jb in jb_list:
                unit_val = ws.cell(r,c).value
                wellpos = len(testnames)+4 #well info stored location
                rowpos = ws.cell(wellpos,c).value[0]
                colpos = ws.cell(wellpos,c).value[1]
                
                try:
                    jb.split('_')[1] #ignore duplicate values
                except Exception:
                    sql_insert_val = "Insert into tsp_test_unit_data.test_unit_data (test_name, julien_barcode, row,col,unit,pillar_plate_id,time) \
                        values ( '{}', '{}', '{}', {}, {},'{}','{}');".format(test,jb,rowpos,colpos,unit_val,ppid,time_now)
                    
                    
                    conn.execute(sql_insert_val)
                    mydb1.commit()
                
                c+=1
            r+=1    
        
        ##Aprove plate
        sql_approve = "UPDATE vibrant_test_tracking.pillar_plate_info set status = 'FINISH', assemble_time = '{}' where pillar_plate_id like '{}';".format(time_now,ppid) #setting status as approve in plate_tracking
        conn.execute(sql_approve)
        mydb1.commit()
        os.remove(os.path.join(path,file_)) #remove additionally created .xlsx file 
        
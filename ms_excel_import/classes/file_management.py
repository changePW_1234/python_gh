#Siddarth M
import glob
import os
import shutil


class file_management_source:
    def __init__(self,source):
        self.source = source
        
    def get_filenames(self): #get names of excel sheets present in source directory
        source = self.source
        os.chdir(source)
        
        size = len(source.split('\\'))
        flist = []
        
        for file in glob.iglob(source + '\\*xlsx', recursive = True):
            f = file.split('\\')[size]
            if f[0] != '~':
                flist.append(f)
            
        return flist

class file_management_destination:
    def __init__(self,filelist,source,destination):
        self.filelist = filelist
        self.destination = destination
        self.source = source
        
    def move_file(self): #moving file to destination path after import
        filelist = self.filelist
        destination = self.destination
        source = self.source           
        try:
            os.mkdir(destination)
        except FileExistsError as e:
            e = str(e)     
        
        for file in filelist:
            shutil.move(source+'\\'+file,destination+'\\'+file)
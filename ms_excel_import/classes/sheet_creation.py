#Siddarth M

from openpyxl import Workbook
from openpyxl.styles import PatternFill, Border, Side
from openpyxl.formatting.rule import CellIsRule, FormulaRule
import os,re
import numpy as np
import math
import datetime as dt

class create_sheet:
    def __init__(self,df, jb_list, collection_time,test_dict,ppid,path):
        self.df = df
        self.jb_list = jb_list
        self.collection_time = collection_time
        self.test_dict = test_dict
        self.ppid = ppid
        self.path = path
    
    #get sheet cell string (row=5,col=3 gives C5)
    def cell_string(self,cell):
        return str(cell).split('.')[1].split('>')[0] 
    
    #split alphanumerically BH100 - (BH,100)
    def split_string(self,cell):
        temp = re.compile("([a-zA-Z]+)([0-9]+)")
        split_tuple = temp.match(cell).groups()
        return split_tuple
    
    
    #sheet creation and formatting
    def create_sheet(self):
        df = self.df
        jb_list = self.jb_list
        collection_time = self.collection_time
        test_dict = self.test_dict
        ppid = self.ppid
        #create sheet
        wb = Workbook()
        ws = wb.active
        ws['A1']='TestCode' ; ws['B1']='low' ; ws['C1']='high' ; ws['B2'] = '(<low*0.2)' ; ws['C2'] = '(>=high*5)'
        ws['D1']='lowPercentage' ; ws['E1']='highPercentage' ;
        ws['F2']='collection time' ; ws['F3']='xy' ; ws['A2'] = ppid
        ws['B3']='dup' ; ws['C3'] ='recent_dup'
        xp = 1 ; yp = 1 ;
        row_1 = 4 ; row_2 = 100 
        r1 = 4 ; r2 = 100 ; low = 2 ; high = 20 ; 
        high_fill = PatternFill(start_color='FFAF02',end_color='FF8402',fill_type='solid') ; v_high_fill = PatternFill(start_color='CB2602',end_color='CB2602',fill_type='solid')
        low_fill = PatternFill(start_color='0288FF',end_color='0288FF',fill_type='solid') ; v_low_fill = PatternFill(start_color='00F7FF',end_color='00F7FF',fill_type='solid')
        dup_fill = PatternFill(start_color='feeda5',end_color='feeda5',fill_type='solid')
        recent_dup_fill = PatternFill(start_color='2AFF00',end_color='2AFF00',fill_type='solid')
        #blank_fill = PatternFill(start_color='FFFFFF',end_color='FFFFFF',fill_type='solid')
        #inserting barcodes in first row and inserting y-Parameter in the second row
        c =7 ; r = 1 ; i =0
        for jb in jb_list:
            ws.cell(column=c, row=r, value = jb) #inserting barcodes
            ws.cell(column=c, row=r+2, value = yp) #inserting y-Parameter
            ws.cell(column=c, row =r+1, value = collection_time[i]) #inserting sample collection time
            try:
                if jb.split('_')[1] == 'Duplicate': 
                    ws[self.cell_string(ws.cell(column=c, row=r))].fill = dup_fill
                    ws[self.cell_string(ws.cell(column=c, row=r+1))].fill = dup_fill
                    
                    #mark recent duplicate samples
                    ct_act = df.loc[(df.jb == jb.split('_')[0])].ctime[0] ; dt_act = dt.datetime.strptime(ct_act,'%Y-%m-%d')
                    ct_dup = df.loc[(df.jb == jb)].ctime[0] ; dt_dup = dt.datetime.strptime(ct_dup,'%Y-%m-%d')
                    difference = (dt_act-dt_dup).days 
                    if difference < 30:
                        ws[self.cell_string(ws.cell(column=c, row=r))].fill = recent_dup_fill
                        ws[self.cell_string(ws.cell(column=c, row=r+1))].fill = recent_dup_fill
                    
            except Exception :
                temp = 0
            c+=1 ; i+=1
        
        #filter based on test name (###confirm if duplicate also has the same test names)
        for test in test_dict:
            df_test = df[(df.test_name == test)]
            low = test_dict[test][1]
            high = test_dict[test][2]
            if high>1e20: #some high values set at 1e308?
                high = 10000000
            ws.cell(column =1, row=r1, value = test) ; #test_name in first column
            ws.cell(column =1, row=r2, value = test) ; #test_name in c1, r101 for raw values 
            ws.cell(column =6, row=r1, value = xp) #insert x-parameter
            ws.cell(column =2, row=r1, value = low) ; low_cell = ws.cell(column=2, row=r1)#insert low value
            ws.cell(column =3, row=r1, value = high) ; high_cell = ws.cell(column=3, row=r1) #insert high value
            r1+=1 ; r2+=1 #iterating to next rows for next test
            c = 7; 
            
            #low-high percentage
            first_cell  = ws.cell(column=7, row = row_1) ; last_cell = ws.cell(column=7+len(jb_list)-1, row = row_1) #first and last cells in jb_list cell range
            first_cell_string = self.cell_string(first_cell)  ; last_cell_string = self.cell_string(last_cell) 
            low_cell_string = self.cell_string(low_cell) ; high_cell_string = self.cell_string(high_cell) #low and high cells
            low_percentage_formula = f'=ROUND((COUNTIF({first_cell_string}:{last_cell_string},"<"&{low_cell_string})/{len(jb_list)})*100,2)&"%"'
            high_percentage_formula = f'=ROUND((COUNTIF({first_cell_string}:{last_cell_string},">"&{high_cell_string})/{len(jb_list)})*100,2)&"%"'
            ws.cell(column=4, row=row_1, value=low_percentage_formula) #insert low percentage formula
            ws.cell(column=5, row=row_1, value=high_percentage_formula) #insert high percentage formula
            #inserting raw values
            for jb in jb_list:
                
                try:
                    unit_val = df_test.loc[jb]['unit']
                except Exception:
                    unit_val = 0 #assigning zero if value dows not exit
                    
                
                well = str(df_test.loc[jb]['well'])
                
                
                ws.cell(column=c, row=row_2, value = float(unit_val)) #raw unit value 
                ws.cell(column=c, row = len(test_dict)+4, value = well)
                #xy-parameter
                current_cell = ws.cell(column=c,row=row_2) ; yParameter = ws.cell(column=c, row=3) ; xParameter = ws.cell(column=6, row= row_1)
                current_cell_string = self.cell_string(current_cell) #getting raw value cell position as a string
                yParameter_string = self.cell_string(yParameter)  #getting y-parameter cell position as a string
                xParameter_string = self.cell_string(xParameter)  #getting x-parameter cell position as a string
                conv_unit_formula = f'=ROUND({current_cell_string}*{xParameter_string}*{yParameter_string},3)' 
                noconv_unit_formula = f'=ROUND({current_cell_string},3)'
                
                
                try:
                    if jb.split('_')[1] == 'Duplicate':
                        ws.cell(column=c, row=row_1, value = noconv_unit_formula) #unconverted value for duplicates
                        
                except Exception:
                    if abs(unit_val)>500000:
                        ws.cell(column=c, row=row_1, value = noconv_unit_formula) #unconverted value for -999999
                    else:
                        ws.cell(column=c, row=row_1, value = conv_unit_formula) #excel formula for converted unit
                
                #np.nan instead of formula for empty cells
                if math.isnan(ws.cell(column=c, row=row_2).value):
                    ws.cell(column=c, row=row_1, value= np.nan)
                
                
                c+=1 #iterating cols for barcode
                
            #formatting
            low_tuple = self.split_string(low_cell_string) ; ls1 = low_tuple[0] ; ls2 = low_tuple [1]
            high_tuple = self.split_string(high_cell_string) ; hs1 = high_tuple[0] ; hs2 = high_tuple[1]
            
            blank_rule = FormulaRule(formula=[f'ISBLANK({first_cell_string})'], stopIfTrue=True) ; 
            low_rule = CellIsRule(operator='lessThan', formula=[f'${ls1}${ls2}'], fill=low_fill) #ex : $B$4
            high_rule = CellIsRule(operator='greaterThan', formula=[f'${hs1}${hs2}'], fill=high_fill)
            v_low_rule = CellIsRule(operator='lessThan', formula=[f'${ls1}${ls2}*0.2'], fill=v_low_fill)
            v_high_rule = CellIsRule(operator='greaterThanOrEqual', formula=[f'${hs1}${hs2}*5'], fill=v_high_fill)
            
            ws.conditional_formatting.add(f'{first_cell_string}:{last_cell_string}',blank_rule)
            ws.conditional_formatting.add(f'{first_cell_string}:{last_cell_string}',v_low_rule)
            ws.conditional_formatting.add(f'{first_cell_string}:{last_cell_string}',v_high_rule)
            ws.conditional_formatting.add(f'{first_cell_string}:{last_cell_string}',low_rule)
            ws.conditional_formatting.add(f'{first_cell_string}:{last_cell_string}',high_rule)
            
    
            row_1+=1 ; row_2+=1
        
        last_val_cell = ws.cell(column=c-1,row=row_1-1)
        last_val_cell_string = self.cell_string(last_val_cell)
        ws['B1'].fill = low_fill ; ws['B2'].fill = v_low_fill
        ws['C1'].fill = high_fill ; ws['C2'].fill = v_high_fill
        ws['B3'].fill = dup_fill ; ws['C3'].fill = recent_dup_fill
        #add freeze panes
        ws.freeze_panes = 'G4'
        
        # #add border
        # cell_range = f'F3:{last_val_cell_string}'
        # border = Border(left=Side(border_style='thin', color='000000'),
        #                 right=Side(border_style='thin', color='000000'),
        #                 top=Side(border_style='thin', color='000000'),
        #                 bottom=Side(border_style='thin', color='000000'))
        
        # rows = ws[cell_range]
        # for row in rows:
        #     for cell in row:
        #         cell.border = border
        
        #ppid_ = ppid.split('_')[0]
        ppid_ = ppid
        os.chdir(self.path) #save path
        
        wb.save(f'{ppid_}_valueConvert.xlsx')

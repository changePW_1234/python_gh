#Siddarth M
import time 
from classes.df_class import df_export,df_import
from classes.sheet_creation import create_sheet
from classes.file_management import file_management_source, file_management_destination

save_path = r'C:\sid_exp\test_folder' #path to save sheet [format - C:\Users\Admin...]
read_path = r'C:\sid_exp\test_folder' #source path to read .xlsx 
move_path = r'C:\sid_exp\test_folder\move_path' #sheet moved to this folder after sql import (will be created by program if it doesn't already exist)

choice = 1 #1 - create sheet, 2-sql import from folder
pillar_plate_arr = ['HMU202104301_HMU_pillar_20210501031637'] #add comma separated pillar plate ids


#find function run time (remove decorator if not needed needed)
def time_decorator(function):
    def wrapper_function(*args,**kwargs):
        ts = time.time()
        result = function(*args,**kwargs)
        tf = time.time()
        print(f'Runtime : {round(tf-ts,2)} seconds')
        return result
    return wrapper_function


#main function for creating sheet
#@time_decorator
def main_sheet(plate_list,path):
    for pillar_plate in plate_list:
        print(f'Running {pillar_plate}...')
        dfClass = df_export(pillar_plate) 
        df_,jb_list_, collection_time_, test_dict_ = dfClass.create_dF()
        sheetClass = create_sheet(df_,jb_list_,collection_time_,test_dict_,pillar_plate,path)
        sheetClass.create_sheet()
    
#main function for importing data from all sheets in the source folder
#@time_decorator
def main_sql(path_1,path_2)  :
    #get file list
    filelist_class = file_management_source(read_path)
    filelist = filelist_class.get_filenames()    
    
    for file_ in filelist:
        print(f'Reading{file_}...')
        dfClass = df_import(path_1,file_)
        dfClass.import_data()
    filelist_ = filelist_class.get_filenames() 
    movefile_class = file_management_destination(filelist_, path_1,path_2)
    movefile_class.move_file()
    
    
#call main
if __name__ == "__main__" :
    if choice == 1: #sheet creation
        main_sheet(pillar_plate_arr,save_path)
    
    if choice == 2: #sql import
        main_sql(read_path,move_path)
        
        
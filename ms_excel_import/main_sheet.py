#Siddarth M

import time
from classes.df_class import df_export
from classes.sheet_creation import create_sheet

pillar_plate_arr = ['HMU202104301_HMU_pillar_20210501031637'] #add comma separated pillar plate ids
path = r'C:\sid_exp\test_folder' #path to save sheet [format - C:\Users\Admin...]

#find function run time (remove decorator if not needed needed)
def time_decorator(function):
    def wrapper_function(*args,**kwargs):
        ts = time.time()
        result = function(*args,**kwargs)
        tf = time.time()
        print(f'Runtime : {round(tf-ts,2)} seconds')
        return result
    return wrapper_function
        
        
if __name__ == "__main__":
    for pillar_plate in pillar_plate_arr:
        print(f'Running {pillar_plate}...')
        dfClass = df_export(pillar_plate) 
        df_,jb_list_, collection_time_, test_dict_ = dfClass.create_dF()
        sheetClass = create_sheet(df_,jb_list_,collection_time_,test_dict_,pillar_plate,path)
        sheetClass.create_sheet() 